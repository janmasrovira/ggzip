# coding: utf-8

#print()
from heapq import heappush, heappop, heapify
from collections import Counter
from bitstring import BitArray
from functools import reduce
import time
from math import ceil, log2
import packageMerge


class Huffman:
    byteAlphabet = [i for i in range(256)]

    def setSymbols2weigths(self, w):
        self.symbols2weigths = w
        
    def getSymbols2weigths(self):
        return self.symbols2weigths

    def setSymbols2lengths(self, d):
        self.symbols2lengths = d
        
    def getSymbols2lengths(self):
        return self.symbols2lengths

    def setAlphabet(self, abc):
        self.abc = abc

    def getDict(self):
        if self.dict is None:
            self.dict = dict(filter(lambda p: len(p[1]) > 0, self.getTable()))
        return self.dict

    def __init__(self):
        self.table = None
        self.tree = None
        self.data = None
        self.dict = None
        self.table = None
        self.abc = None
        self.symbols2weigths = None
        self.symbols2lengths = None
        self.lengths = None

    def fromLengths(lengths, alphabet):
        if len(alphabet) < len(lengths):
            raise Exception("Error: len(alphabet) < len(lengths)")
        H = Huffman()
        H.setAlphabet(alphabet)
        H.lengths = lengths + (len(alphabet) - len(lengths))*[0]
        return H
        
    def fromData(data, LengthLimit = 0, alphabet = None):
        H = Huffman()
        if alphabet is None: alphabet = Huffman.byteAlphabet
        H.setSymbols2weigths(dict(Counter(data)))
        H.setAlphabet(alphabet)
        H.calcLengths(LengthLimit)
        H.data = data
        H.checkAlphabet()
        return H

    def checkLengths(self, maxLength):
        for l in self.getLengths():
            if l > maxLength:
                return False
        return True
            
    def checkAlphabet(self):
        if not set(self.getSymbols2weigths().keys()).issubset(set(self.getAlphabet())):
            raise Exception("An invalid alphabet was given")

    def updateLengthList(self):
        d = self.getSymbols2lengths()
        self.lengths = []
        for sym in self.getAlphabet():
            self.lengths += [d[sym]] if sym in d else [0]
        
    def calcLengths(self, Limit = 0):
        self.calcLengthsUnlimited()
        if Limit > 0 and not self.checkLengths(Limit):
            self.calcLengthsLimited(Limit)
       
            
    def calcLengthsLimited(self, Limit):
        if Limit < ceil(log2(len(self.symbols2weigths))):
            raise Exception("No solution exists, Limit too low")
        I = []
        index = dict([(ix, sym_w) for ix, sym_w in enumerate(self.symbols2weigths.items())])
        for d in range(1, Limit  + 1):
            for ix, sym_w in index.items():
                I += [packageMerge.Item(2**-d, sym_w[1], ix)]
        s = packageMerge.solve(X = len(self.getSymbols2weigths()) - 1, I = I)
        self.symbols2lengths = dict(Counter(map(lambda i: index[i.index][0],
                                                packageMerge.Item.mapUnpack(s))).items())
        self.updateLengthList()
        
    def calcLengthsUnlimited(self):
        stw = self.getSymbols2weigths()
        if len(stw) == 1:
            symbol = list(stw.keys())[0]
            self.setSymbols2lengths(dict([(symbol, 1)]))
            self.updateLengthList()
            return
        heap = [ [weight, [sym, 0]] for sym, weight in stw.items()]
        heapify(heap)
        addBit = lambda l: [ [sym, lenght + 1] for sym, lenght in l[1:] ]
        while len(heap) > 1:
            left = heappop(heap)
            right = heappop(heap)
            heappush(heap, [left[0] + right[0]] + addBit(left) + addBit(right))
        self.setSymbols2lengths(dict(heappop(heap)[1:]))
        self.updateLengthList()
        
    def getAlphabet(self):
        return self.abc

    def getLengths(self, zeros = True):
        return self.lengths if zeros else list(filter(lambda x: x > 0, self.lengths))

    def getTable(self):
        if self.table is None:
            self._calcTable()
        return self.table

    def printTable(self):
        print("\n\tSymbol\tCode")
        print("\t------\t----")
        for sym, code in self.getTable():
            if len(code) > 0:
                print("\t%s\t%s"%(str(sym),code.bin))
        
    def _calcTable(self):
        lengths = self.getLengths()
        max_length = max(lengths)
        ''' 
        comptem per cada longitud de bits, quants codis té associada
        en aquest cas, les longituds 1, 2 tenen un codi cadascuna, la longitud 3 en té dos
        '''
        bl_count = Counter(lengths)
        bl_count[0] = 0
        next_code = [0]
        for bits in range(1, max_length + 1):
            next_code += [(next_code[-1] + bl_count[bits - 1])*2]        
        '''
        ara toca assignar valors als codis fent servir valors consecutius a partir dels valors inicials
        calculats anteriorment        
        '''
        codes = []
        for length in lengths:
            if length != 0:
                codes += [BitArray(uint = next_code[length], length = length)]
                next_code[length] += 1
            else:
                codes += [BitArray()] # vol dir que no se li assigna cap codi
        '''
        ja podem contruir la taula
        '''
        self.table = []
        for sym, code in zip(self.getAlphabet(), codes):
            self.table += [(sym, code)]
            
    def encode(self, otherData = None, outFile = None):
        data = self.data if otherData is None else self.data
        dtable = self.getDict() 
        e = BitArray()
        for c in data:
            e.append(dtable[c])
        if outFile is not None:
            with open(outFile, 'wb') as f:
                f.write(e.tobytes())
        return e

    def encodeSymbol(self, symbol):
        return self.getDict()[symbol]

    '''
    Per poder descodificar eficientment el text codificat podem fer servir un arbre prefix.
    Com que és un codi prefix, els símbols estaran a les fulles de l'arbre
    '''
    class Node(object):
        def __init__(self):
            self.zero = None
            self.one = None
            self.symbol = None
            
        def is_leaf(self):
            return self.symbol != None

        def child(self, bit):
            if bit != '0' and bit != '1': raise Exception("Invalid bit: " + bit)
            return self.zero if bit == '0' else self.one

        '''
        key ha de ser una string de 0s i 1s
        '''
        def insert(self, key, symbol):
            if key == '':
                self.symbol = symbol
            elif key[0] == '0':
                if self.zero == None:
                    self.zero = Huffman.Node()
                self.zero.insert(key[1:], symbol)
            elif key[0] == '1':
                if self.one == None:
                    self.one = Huffman.Node()
                self.one.insert(key[1:], symbol)
            else:
                raise Exception("Unexpected key character: " + key)

    
    '''
    Pre: root mai serà fulla
    '''
    class Tree(object):
        def __init__(self):
            self.root = Huffman.Node() 
            self.pointer = self.root
        
        def insert(self, key, symbol):
            self.root.insert(key, symbol)

        def reset(self):
            self.pointer = self.root

        def feed(self, bit):
            self.pointer = self.pointer.child(bit)
            if self.pointer.is_leaf():
                r = self.pointer.symbol
                self.pointer = self.root 
            else:
                r = None
            return r

    def getTree(self):
        if self.tree is None:
            self.tree = Huffman.Tree()
            for sym, code in self.getDict().items():
                self.tree.insert(code.bin, sym)
        return self.tree

    def decode(self, encodedBits):
        #falta llegir la mida del block (no s'haurien de descodificat els bits sobrants (padding))
        tree = self.getTree()
        decoded_data = []
        for b in encodedBits.bin:
            x = tree.feed(b)
            if x != None: decoded_data += [x]
        return decoded_data

    def decodeFirst(self, encodedBits):
        tree = self.getTree()
        tree.reset()
        for i, b in enumerate(encodedBits.bin):
            x = tree.feed(b)
            if x is not None:
                return i + 1, x
        return None
