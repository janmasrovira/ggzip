# coding: utf-8

import huffman
import lz77
import codeLengths
import imp
import deflate
import filecmp
import gzip
from os.path import getsize
from collections import Counter
import time

# imp.reload(huffman)
imp.reload(lz77)
imp.reload(codeLengths)
imp.reload(deflate)

wut = "data/wut.txt"
out = "data/out.txt.tmp"
carro = "data/carro.txt"
copro = "data/copro.txt"
dgzip = "data/gzip.txt"
blah = "data/blah.txt"


def calcBits(data, symbol2length):
    return sum(symbol2length[c] for c in data)

def perCent(numerator, denominator):
    return numerator/denominator*100

def testSeparator():
    print("")
    print("+"+ 80*"-" + "+")
    print("+"+ 80*"-" + "+")
    print("\n")


def title(txt):
    width = 100
    hhalf = 2
    txt = ".: " + txt + " :."
    lines = "+" + width*"-" + "+"
    space = "|" + width*" " + "|"
    print(lines)
    for _ in range(hhalf):
        print(space)
    l = list(space)
    n = len(txt)
    i = (width - n)//2 
    l[i: i + n] = txt
    print(''.join(l))
    for _ in range(hhalf):
        print(space)
    print(lines)
 
def testHuffmanEncoding(file = dgzip):
    with open(file, 'rb') as f:
        data = f.read()
    print("TESTING " + file + "\n")
        
    print("beggining of original data (total: %d bytes):"%(len(data)))
    print(data[:70])
        
    H = huffman.Huffman.fromData(data)
    print("\nHuffman Table:")
    H.printTable()

    start = time.time()
    e = H.encode(outFile = out)
    print("\nencoding time: %0.4f"%(time.time() - start))
        
    print("\nbeggining of encoded data (total: %d bits):"%(len(e)))
    print(e[:30])
    print("\ncompression ratio: %0.5f"%((len(data)/len(e)*8))) 
        
    start = time.time()
    d = bytes(H.decode(e))
    print("decoding time: %0.4f"%(time.time() - start))

    print("\nbeggining of decoded data:")
    print(d[:70])
    testSeparator()



def testHuffmanLengthLimit(file, L = 15):
    with open(file, 'rb') as f:
        t = f.read()
        
    print("TESTING " + file + "\n")
    c = Counter(t)
    print("Unlimited Length:")
    H = huffman.Huffman.fromData(t, 0, list(set(t)))
    l = H.symbols2lengths
    print("lengths of the huffman codes:", sorted(l.values()))
    ba = calcBits(t, l)
    print("encoded data's number of bits ->", ba)
    
    print("-------------\n")
    print("Limited Length: (Maximum length = %d)"%(L))
    H = huffman.Huffman.fromData(Counter(t), L)
    l = H.symbols2lengths
    print("lengths of the huffman codes:", sorted(l.values()))
    bb = calcBits(t, l)
    print("encoded data's number of bits ->", calcBits(t, l))
    
    print("\nAn extra %0.4f%% of bits is needed in the length limited huffman version"%(bb/ba*100-100))
    testSeparator()


def testLZ77(file, out = None):
    if out is None:
        out = file + ".lz77"
    with open(file, 'rb') as f:
        data = f.read()
    print("TESTING " + file + "\n")
    compressor = lz77.LZ77()
    start_time = time.time()
    l = compressor.compress(data, verbose = True)
    print("Execution time: %0.4f seconds" % (time.time() - start_time))
    print("The output has been saved in the file " + out)
    with open(out, 'w') as o:
        o.write(str(l))
    testSeparator()
        

def testCodeLengths(file):
    with open(file, 'rb') as f:
        data = f.read()
    print("TESTING " + file + "\n")
    H = huffman.Huffman.fromData(data, LengthLimit = 15)
    lengths = H.getLengths()
    l1 = len(lengths)*4
   
    print("Using bytes as alphabet (0..255), the lengths of the huffman tree for our file are the following:")
    print(lengths)
    rle = codeLengths.encodeLengthsToRLE(lengths)
    print("\nRLE encoding:")
    print(rle)

    bits, huff = codeLengths.encodeRLEToBits(rle)
    l2 = len(bits.bin)
    l3 = l2 + 4 + 19*3
    print("\nNow we encode the numbers (singles and the firsts of the tuples) with a new huffman tree, this tree will have to be stored as well.\nThen we put all the bits together:\n", bits.bin)

    print("\nWith the help of the tree calculated in the previous step we can get the original lengths back:")
    lengths_ = codeLengths.decode(bits, huff)
    print(lengths_)
    print("\nCheck that the length sequences are identical:", "YES" if lengths == lengths_ else "NO")

    print("\nWithout the codeLength encoder we would have used a total of %d bits to represent the length sequence (4 bits per length, since they are in range of 0..15).\nUsing the codeLength encoder we only used %d bits. However, we must add the bits needed to encode the auxiliary huffman tree. This tree will need at most 4 + 19*3 = 61 bits, this makes a total of %d bits which is a %0.3f%% of the non-compressed version"%(l1, l2, l3, perCent(l3,l1)))
    testSeparator()

def compressGzip(file):
    out = file + ".gz"
    with open(file, 'rb') as f_in: 
        with gzip.open(out, 'wb') as f_out:
            f_out.writelines(f_in)
    return out

def decompressGzip(file):
    out = file + ".dec"
    with gzip.open(file, 'rb') as f_in: 
        with open(out, 'wb') as f_out:
            f_out.writelines(f_in)
    return out

    
            
def testDeflate(file):
    print("TESTING " + file + "\n")
    osize = getsize(file)
    print("Original file size: %d Bytes\n"%(osize))

    t0 = time.time()
    comp = compressGzip(file)
    t1 = time.time()
    csize = getsize(comp)
    print("Gzip implementation:")
    print("Compressed file size: %d Bytes"%(csize))
    print("Compression ratio: %0.4f"%(osize/csize)) #wiki compression ratio
    print("Compression execution time: %0.4fs"%(t1-t0))
    t0 = time.time()
    decomp = decompressGzip(comp)
    t1 = time.time()
    print("Decompression execution time: %0.4fs"%(t1-t0))
    print()

    

    t0 = time.time()
    print ("Execution times for each block:")
    comp = deflate.compressFile(file, verbose = True)
    t1 = time.time()
    csize = getsize(comp)
    print("Our implementation:")
    print("Compressed file size: %d Bytes"%(csize))
    print("Compression ratio: %0.4f"%(osize/csize)) #wiki compression ratio
    print("Compression execution time: %0.4fs"%(t1-t0))
    t0 = time.time()
    decomp = deflate.decompressFile(file)
    t1 = time.time()
    print("Decompression execution time: %0.4fs"%(t1-t0))
    print("is the original file equal to the decompressed one? " +
          ("YES" if filecmp.cmp(file, decomp) else "NO"))
    testSeparator()

    
def testsHuffmanEncoding():
    title("HUFFMAN ENCODING TESTS")
    testHuffmanEncoding(blah)
    testHuffmanEncoding(dgzip)
    testHuffmanEncoding(copro)

def testsHuffmanLengthLimit():
    title("LENGTH LIMITED HUFFFMAN TESTS")
    testHuffmanLengthLimit(dgzip, 6)
    #testFile(gzip, 5) #no té solució
    testHuffmanLengthLimit(copro, 7)
    testHuffmanLengthLimit(wut, 8)
    
def testsLZ77():
    title("LZ77 TESTS")
    testLZ77(blah)
    testLZ77(copro)
 

def testsCodeLengths():
    title("CODE LENGTH SEQUENCE ENCODING TESTS")
    testCodeLengths(dgzip)
    testCodeLengths(copro)
    
def testsDeflate():
    title("DEFLATE TESTS")
    testDeflate(dgzip)
    testDeflate(copro)
    testDeflate(wut)

testsHuffmanEncoding()
testsHuffmanLengthLimit()
testsLZ77()
testsCodeLengths()
testsDeflate()

