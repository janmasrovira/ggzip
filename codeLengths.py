# coding: utf-8
print()
import huffman
import imp
from bitstring import BitArray

imp.reload(huffman)
    
alphabet = (16,17,18,0,8,7,9,6,10,5,11,4,12,3,13,2,14,1,15)
extraBits = dict([(i, 0) for i in range(16)] + [(16, 2), (17,3), (18,7)])
base = dict([(16,3), (17, 3), (18, 11)])
    
'''
codifica una seqüència de longituds de codi huffman (0-15) en una 
nova seqüència més compacte (0-18)  
'''
def encodeLengthsToRLE(lengths):
    i = 0
    L = len(lengths)
    s = []
    while i < L:
        l = lengths[i]
        if l == 0:
            z = 1
            while  i + z < L and z < 138 and lengths[i + z] == 0:
                z += 1
            i += z
            if z < 3:
                s += z*[0]
            elif 3 <= z <= 10:
                s += [(17, BitArray(uint = z - base[17], length = extraBits[17]))]
            elif 11 <= z <= 138:
                s += [(18, BitArray(uint = z - base[18], length = extraBits[18]))]
        else:
            s += [l]
            k = 0
            while i + k  + 1 < L and lengths[i + k + 1] == l:
                k += 1
            i += k + 1
            while k >= 3:
                h = min(6, k)
                k -= h
                s += [(16, BitArray(uint = h - base[16], length = extraBits[16]))]
            s += k*[l]
    return s
        
def encodeRLEToBits(rle):
    '''
    first encode the numbers using huffman encoding
    '''
    nums = [x if isinstance(x, int) else x[0] for x in rle] # list of numbers in range of [0,18]
    H = huffman.Huffman.fromData(data = nums, alphabet = alphabet, LengthLimit = 7)
    d = H.getDict()
    bits = BitArray()
    '''
    put all the bits in a BitArray
    '''
    for x in rle:
        if isinstance(x, int):
            bits += d[x]
        elif isinstance(x, tuple):
            a, b = x
            bits += d[a] + b
        else:
            raise Exception("Expected an int or a tuple")
    return (bits, H)

def decodeBitsToRLE(bits, huff, n = -1):
    limit = n != -1
    rle = []
    while True:
        x = huff.decodeFirst(bits)
        if limit:
            if n == 0:
                return rle
            if n < 0:
                raise Exception("error, could not decode the exact number of lengths, n = %d"%(n))
     
        if x is None:
            if limit:
                raise Exception("error")
            return rle
        i, a = x
        del bits[:i]
        if a <= 15:
            rle += [a]
            n -= 1
        else:
            j = extraBits[a]
            b = bits[:j]
            del bits[:j]
            rle += [(a, b)]
            if limit:
                base = 3 if a == 16 or a == 17 else 11
                n -= base + b.uint
                
            
        
def decodeRLEToLengths(rle):
    dec = []
    for x in rle:
        if isinstance(x, int):
            dec += [x]
        elif isinstance(x, tuple):
            a, b = x
            if a == 16:
                dec += (base[a] + b.uint)*[dec[-1]]
            elif a == 17 or a == 18:
                dec += (base[a] + b.uint)*[0]
    return dec


def encode(lengths):
    rle = encodeLengthsToRLE(lengths)
    bits, huff = encodeRLEToBits(rle)
    return (bits, huff)

def decode(bits, huff, n = -1):
    rle = decodeBitsToRLE(bits, huff, n)
    lengths = decodeRLEToLengths(rle)
    return lengths


    
