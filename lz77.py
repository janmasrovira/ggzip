# coding: utf-8

from itertools import cycle, takewhile
from collections import defaultdict
import time

def cycleIx(a, b, i):
    return a + i%(b - a + 1)

class LZ77:

    '''
    size indica el nombre de llistes de la taula
    s'ignoren els elements que passen la posició maxLen 
    '''
    def __init__(self, tableSize = 2**16 , minLen = 3, maxLen = 2**8, maxHashMatches = 2**6, cutLength = 15, window = 2**15):
        #self.table = [[] for _ in range(tableSize)]
        self.table = defaultdict(list)
        self.maxLen = maxLen
        self.minLen = minLen
        self.size = tableSize
        self.data = None
        self.window = window
        self.cutLength = cutLength
        self.maxHashMatches = maxHashMatches
      
        
    def index_(self, xyz):
        return hash(xyz)%self.size

    def index(self, xyz):
        return xyz[0] + xyz[1]*256 + xyz[2]*256**2 
    
    class Match:
        def __init__(self, start, position, length = 0):
            self.start = start
            self.length = length
            self.distance = position - start
        def __gt__(self, match):
            return self.length > match.length
        def __str__(self):
            return "(Length = %d, Distance = %d)"%(self.length, self.distance)

        
    def longestMatch(self, i):
        data = self.data
        xyz = data[i : i + 3]
        hashMatches = self.table[self.index(xyz)]
        longest = self.Match(0,0)
        ix = 0
        while ix < min(len(hashMatches), self.maxHashMatches) and i - hashMatches[ix] < self.window:
            start = hashMatches[ix]
            if start == i:
                ix += 1
                continue
            ix += 1
            current = self.Match(start, i)
            j = 0
            while i + j < len(data) and j < self.maxLen and data[i + j] == data[cycleIx(start, i, j)]:
                j += 1
            current.length = j
            longest = max(longest, current)
            if longest.length > self.cutLength:
                break
        return longest
   
    #alternative implementation with less indexes -> cute but inefficient
    def longestMatch_(self, i):
        xyz = self.data[i : i + 3]
        hashMatches = self.table[self.index(xyz)][:self.maxHashMatches]
        longest = self.Match(0,0)
        for start in takewhile(lambda st: i - st < self.window, hashMatches):
            current = self.Match(start, i)
            pdata = cycle(self.data[start:i])
            current.length = len(list(takewhile(lambda p: p[0] == p[1],
                                                zip(pdata, self.data[i: i + self.maxLen]))))
            longest = max(longest, current)
            if longest.length > self.cutLength:
                break
        return longest
        
    def putLiteral(self, b):
        self.lits += 1
        self.compressed += [b]

    def putPointer(self, distance, length):
        self.pointers += 1
        self.compressed += [(length, distance)]
    

    def _compressInit(self, data):
        self.data = data
        self.lits = 0
        self.pointers = 0
        self.compressed = []
     
    def compress(self, data, verbose = False):
        self._compressInit(data)
        L = len(data)
        i = 0
        while i + 3 <= L:
            if i % 3000 == 0 and verbose:
                print("%0.3f %%"%(100*i/L))
            ix = self.index(data[i: i + 3])
            self.table[ix].insert(0, i) 
            m = self.longestMatch(i)
            if m.length < self.minLen:
                self.putLiteral(data[i])
                i += 1
            else:
                self.putPointer(m.distance, m.length)
                i += m.length
        while i < L:
            self.putLiteral(data[i])
            i += 1
        if verbose:
            print("literals = ", self.lits, "; pointers =", self.pointers)
        return self.compressed
