# coding: utf-8
print()
from collections import Counter
import huffman
from math import log2, ceil
from itertools import groupby

def merge(A, B):
    R = []
    na, nb = len(A),len(B)
    ia = ib = 0
    while ia < na and ib < nb:
        if A[ia] < B[ib]:
            R += [A[ia]]
            ia += 1
        else:
            R += [B[ib]]
            ib += 1
    R += A[ia:] + B[ib:]
    return R

def package(A):
    R = []
    n = len(A)
    for i in range(n//2):
        R += [A[2*i] + A[2*i + 1]]
    return R

class Item:

    def mapUnpack(lst):
        u = []
        for i in lst:
            u += i.unpack()
        return u
    
    def __init__(self, width = 0, weight = 0, index = -1):
        self.width = width
        self.weight = weight
        self.package = [self]
        self.index = index
        
    def __lt__(self, item):
        return (self.weight, self.index) < (item.weight, item.index)

    def __eq__(self, item):
        return list(self) == list(item)
    
    def __add__(self, item):
        c = Item(self.width + item.width, self.weight + item.weight)
        c.package = self.package + item.package
        return c
    
    def __repr__(self):
        return str(list(self))

    def __hash__(self):
        return hash((self.width, self.weight, self.index))

    def __iter__(self):
        return iter([self.width, self.weight, self.index])

    def unpack(self):
        u = []
        for i in self.package:
            u += [i]
        return u
        
'''
Expansió de x com a suma de potències de 2, es retorna la llista dels eponents de petit a gran
'''
def dyadicExpansion(x):
    i, d = divmod(x, 1)
    i = int(i)
    exp = []
    #part entera
    rbi = list(reversed(bin(i)[2:]))
    exp = [e for e, _ in filter(lambda p: p[1] == '1', enumerate(rbi))]
    # part decimal
    # 0 <= d < 1
    def dyadicExpansionDecimal(d, p = -1):
        if d == 0 or p < -20:
            return []
        if d >= 2**p:
            return dyadicExpansionDecimal(d - 2**p, p - 1) + [p]
        else:
            return dyadicExpansionDecimal(d, p - 1)
    exp = dyadicExpansionDecimal(d) + exp
    return exp
        
def calcMinWidth(X):
    return 2**dyadicExpansion(X)[0]
    
    
# def huffmanLL(counter, LM):
#     if LM < ceil(log2(len(counter))):
#         raise Exception("No solution exists, LM too low")
#     I = []
#     index = dict([(ix, sym_w) for ix, sym_w in enumerate(counter.items())])
#     for d in range(1, LM  + 1):
#         for ix, sym_w in index.items():
#             I += [Item(2**-d, sym_w[1], ix)]
#     s = solve(X = len(counter) - 1, I = I)
#     return dict(Counter(map(lambda i: index[i.index][0], Item.mapUnpack(s))).items())


def initializeL(I):
    L = []
    bywidth = groupby(sorted(I, key = lambda i: i.width), key = lambda i: i.width)
    for width, group in bywidth:
        g = []
        for i in group:
            g += [i]
        L += [sorted(g)]
    return L
        
    
def solve(X, I):
    L = initializeL(I)
    S = []
    while X > 0:
        minWidth = calcMinWidth(X)
        if L == []:
            return None
        Ld = L.pop(0)
        r = Ld[0].width
        if r > minWidth:
            return None
        elif r == minWidth:
            S += [Ld.pop(0)]
            X -= r
        P = package(Ld)
        if L != []:
            L[0] = merge(P, L[0])
        else:
            L = [P]
    return S



