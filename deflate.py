# coding: utf-8

print()

import huffman
import codeLengths
from imp import reload
import time
reload(huffman)
reload(codeLengths)

from huffman import Huffman
import codeLengths
from lz77 import LZ77
from bitstring import BitArray
from itertools import cycle, islice


	
blockSize = 2**15
minLength = 3
maxLength = 258 #deflate limit
END = 256
litAlphabet = list(range(285 +  1))
distAlphabet = list(range(29 + 1))

    
def distanceBase(i):
    return (1,2,3,4,5,7,9,13,17,25,33,49,65,97,129,193,257,385,513,769,1025,1537,2049,3073,4097,6145,8193,12289,16385,24577)[i]

def lengthBase(i):
    return (3,4,5,6,7,8,9,10,11,13,15,17,19,23,27,31,35,43,51,59,67,83,99,115,131,163,195,227,258)[i-257]

def lengthCode(length):
    if not minLength <= length <= maxLength:
        raise Exception("Length out of range")
    if length <= 10:
        return 257 + length - 3
    elif length <= 18:
        return 265 + (length - 11)//2
    elif length <= 34:
        return 269 + (length - 19)//4
    elif length <= 66:
        return 273 + (length - 35)//8
    elif length <= 130:
        return 277 + (length  - 67)//16
    elif length <= 257:
        return 281 + (length - 131)//32
    return 258

def distanceCode(dist):
    if not 1 <= dist <= 2**15:
        raise Exception("invalid distance %d"%(dist))
    i = 0
    while i < 30 and distanceBase(i) <= dist: i += 1
    return i - 1

def extraDistanceBits(code):
    if 0 <= code <= 1:
        return 0
    elif 2 <= code <= 29:
        return (code >> 1) - 1
    else:
        raise Exception("illegal distance code: " + str(code))

def extraLengthBits(code):
    if 257 <= code <= 260 or code == 285:
        return 0
    elif 261 <= code <= 284:
        return ((code - 257) >> 2) - 1
    else:
        raise "illegal length code"

def splitLZ(lz):
    litslens = []
    dists = []
    codesNbitslz = []
    for x in lz:
        if isinstance(x, int):
            litslens += [x]
            codesNbitslz += [x]
        else:
            l = x[0]
            d = x[1]
            lc = lengthCode(l)
            lb = lengthBase(lc)
            le = extraLengthBits(lc)
            
            dc = distanceCode(d)
            db = distanceBase(dc)
            de = extraDistanceBits(dc)
            litslens += [lc]
            dists += [dc]
            codesNbitslz += [lc]
            if le > 0:
                codesNbitslz += [BitArray(uint = l - lb, length = le)]
            codesNbitslz += [dc]
            if de > 0:
                #print("d dc db de", d, dc, db, de)
                codesNbitslz += [BitArray(uint = d - db, length = de)]
    return litslens, dists, codesNbitslz
                    

'''
removes zeros at the end of a sequence
'''
def removeTrailingZeros(lengths):
    _lengths = lengths[:]
    while _lengths != [] and _lengths[-1] == 0: _lengths.pop(-1)
    return _lengths

def list2bits(l, bitsPerElem):
    bits = BitArray()
    for x in l:
        bits += BitArray(uint = x, length = bitsPerElem)
    return bits

def bits2list(b, bitsPerElem):
    _b = b[:]
    l = []
    while len(_b) > 0:
        l += [popBits(_b, bitsPerElem).uint]
    return l
            
def isLength(code):
    return END < code

def isLiteral(code):
    return code <= 255

def isEnd(code):
    return code == END
    
def compressBlock(block, verbose = False):
    LZcompressor = LZ77(minLen = minLength, maxLen = maxLength,
                        window = blockSize)    
    t0 = time.time()
    lz = LZcompressor.compress(block)
    t1 = time.time()
    if verbose:
        print("LZ77: %0.4f seconds"%(t1-t0))
    
    litsCodes, distCodes, codeslz = splitLZ(lz)
    if distCodes == []: distCodes = [0]
        
    huffLit = Huffman.fromData(data = litsCodes + [END],
                               alphabet = litAlphabet, LengthLimit = 15)
    huffDist = Huffman.fromData(data = distCodes, alphabet = distAlphabet)

    litLengths = removeTrailingZeros(huffLit.getLengths())
    distLengths = removeTrailingZeros(huffDist.getLengths())
        
    huffLitDistBits, huffCLen = codeLengths.encode(litLengths + distLengths)
    clenLengths = removeTrailingZeros(huffCLen.getLengths())

    HCLEN = BitArray(uint = len(clenLengths) - 4, length = 4)
    HLIT = BitArray(uint = len(litLengths) - 257, length = 5)
    HDIST = BitArray(uint = len(distLengths) - 1, length = 5)

    bits = HLIT + HDIST + HCLEN

    bits += list2bits(clenLengths, 3)
    bits += huffLitDistBits

    t2 = time.time()
    if verbose:
        print("huffman, codelengths, header: %0.4f seconds"%(t2 - t1))
    '''
    data compression using huffLit and huffDist
    '''
    i = 0
    while i < len(codeslz):
        code = codeslz[i]
        i += 1
        bits += huffLit.encodeSymbol(code)
        if isLength(code):
            le = extraLengthBits(code)
            if le > 0:
                bits += codeslz[i]
                i += 1
            dc = codeslz[i]
            i += 1
            bits += huffDist.encodeSymbol(dc)
            de = extraDistanceBits(dc)
            if de > 0:
                bits += codeslz[i]
                i += 1

    bits += huffLit.encodeSymbol(END)
    t3 = time.time()
    if verbose:
        print("encoding: %0.4f seconds"%(t3-t2) + "\n")
    return bits

def decompress(bits):
    D = []
    P = []
    while len(bits) > 0:
        P = decompressBlock(bits, P)
        D += P
    return D
            
def decompressBlock(bits, P):
    HLIT = popBits(bits, 5)
    HDIST = popBits(bits, 5)
    HCLEN = popBits(bits, 4)

    huffCLenLens = bits2list(popBits(bits, 3*(HCLEN.uint + 4)), 3)
    huffCLen = Huffman.fromLengths(lengths = huffCLenLens, alphabet = codeLengths.alphabet)

        
    huffLitDistsLens = codeLengths.decode(bits, huffCLen, HLIT.uint + 257 + HDIST.uint + 1)
        
    huffLitLens = huffLitDistsLens[:HLIT.uint + 257]
    huffDistLens = huffLitDistsLens[HLIT.uint + 257:]
        
    huffLit = Huffman.fromLengths(lengths = huffLitLens, alphabet = litAlphabet)
    huffDist = Huffman.fromLengths(lengths = huffDistLens, alphabet = litAlphabet)

    D = []
    PD = P
    while True:
        i, code = huffLit.decodeFirst(bits)
        popBits(bits, i)
        if isEnd(code):
            return D
        elif isLiteral(code):
            D += [code]
            PD += [code]
        elif isLength(code):
            le = extraLengthBits(code)
            lb = lengthBase(code)
            add = popBits(bits, le).uint if le > 0 else 0
            length = lb + add
            
            i, dc = huffDist.decodeFirst(bits)
            popBits(bits, i)
            de = extraDistanceBits(dc)
            db = distanceBase(dc)
            add = popBits(bits, de).uint if de > 0 else 0
            dist = db + add

            slice = islice(cycle(PD[-dist:]), 0, length)
            D += slice
            PD += slice
        else:
            raise Exception("Unexpected code")
        
def popBits(bitarray, n):
    p = bitarray[:n]
    del bitarray[:n]
    return p
        
def compress(data, verbose = False):
    bits = BitArray()       
    while len(data) > 0:
        bdata = data[:blockSize]
        data = data[blockSize:]
        bits += compressBlock(bdata, verbose)
    return bits

def compressFile(infile, outfile = None, verbose = False):
    if outfile is None:
        outfile = infile + ".ggzip"
    with open(infile, 'rb') as inp:
        data = inp.read()
    bits = compress(data, verbose)
    with open(outfile, 'wb') as out:
        bits.tofile(out)
    return outfile

def decompressFile(infile, outfile = None):
    if outfile == None:
        outfile = infile + ".dec"
    bits = BitArray(filename = infile)
    with open(outfile, 'wb') as out:
        bits.tofile(out)
    return outfile
        
